homemade db
=========

A data layer node module providing models and methods to all projects

## Installation

```shell
  npm install midsea-db --save
```

## Usage

```js
  var db = require('midsea-db')
      query = db.Platform.find({...});

```

## Tests

```shell
   npm test
```

## Release History

* 0.0.1 Initial release
