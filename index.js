var keystone = require('keystone'),
    path = require('path');

module.exports = function(mongoose) {
	keystone.connect(mongoose);
	require('./models');

	return {
		keystone: keystone,
		mongoose: mongoose,
		user: keystone.lists.User,
		platform: keystone.lists.Platform,
		platformType: keystone.lists.PlatformType,
		location: keystone.lists.Location,
        address: keystone.lists.Address,
		driver: keystone.lists.Driver,
		order: keystone.lists.Order,
		orderDetail: keystone.lists.OrderDetail
	};
};