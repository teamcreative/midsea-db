var mongoose = require('mongoose'),
    env = require('dotenv'),
    async = require('async'),
    should = require('chai').should(),
    assert = require('chai').assert,
    expect = require('chai').expect;

env.load();

//mongoose.connect(process.env.MONGO_URI);
var db = require('../index')(mongoose.connect('mongodb://localhost/test'));

db.mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

db.mongoose.connection.once('open', function callback() {
    //console.log('open');
});


describe('hooks', function(){

    beforeEach(function(done) {
        async.series([

            function (cb) {

                db.location.model.remove(function (err) {

                    db.user.model.remove(function (err) {
                        db.driver.model.remove(function (err) {
                            db.order.model.remove(function (err) {
                                db.orderDetail.model.remove(function (err) {
                                    cb(err);
                                });
                            });
                        });
                    });
                });
            },

            function (cb) {

                var user = new db.user.model();
                user.name.first = "Sam";
                user.name.last = "Jones";
                user.email = "ali.hmer@gmail.com";
                user.username = "flash";
                user.password = "password";
                user.phone = "3063517173";
                user.verifications.email = true;
                user.verifications.phone = true;

                user.save(function (err, saved) {
                    cb(err, saved);
                });

            },
            function (cb) {

                db.user.model.findOne()
                    .exec(function (err, user) {

                        var location = new db.location.model();
                        location.name = 'home';
                        location.user = user;
                        location.primary = true;
                        location.place.number = "2600";
                        location.place.street1 = "ROBINSON STREET";
                        location.place.suburb = "Regina";
                        location.place.state = "Saskatchewan";
                        location.place.postcode = "S4T2R4";
                        location.place.country = "Canada";
                        
                        location.place.geo = [100,100];

                        location.save(function (err, saved) {

                            cb(err, saved);

                        });

                    });

            },
            function (cb) {

                var driver = new db.driver.model();
                driver.name.first = "Sonny";
                driver.name.last = "Hooper";
                driver.email = "ali.hmer@gmail.com";
                driver.state = "valid";
                driver.calendarHandle = "Sonny Hooper";
                driver.phone = "3063517173";
                driver.startDate = new Date();

                driver.save(function (err, saved) {

                    saved.email.should.eql(driver.email);

                    cb(err, saved);
                });

            },
            function (cb) {

                db.driver.model.findOne()
                    .exec(function (err, driver) {

                        db.user.model.findOne()
                            .populate('locations')
                            .exec(function (err, user) {

                                var order = new db.order.model();
                                order.scheduledPickupDriver = driver;
                                order.who = user;
                                order.location = user.locations[0];

                                order.save(function (err, saved) {

                                    cb(err);


                                });

                            });

                    });

            }

        ], function (err, results) {

            if (err) {

                assert.isNull(err, 'No errors');
            }

            done();


        });
    })
    after(function(){
        // runs after all tests in this block
    })
    before(function(){
        // runs before each test in this block
    })
    afterEach(function(){
        // runs after each test in this block
    })


    it('view order', function (done) {

        async.series([

            function (cb) {

                db.order.model.findOne()
                    .populate('orderDetails')
                    .exec(function (err, order) {

                        assert.equal(order.orderDetails.length,1, 'should have one detail');
                        assert.equal(order.orderDetails[0].state, 'placed', 'should be placed');
                        order.getCurrentState(function(state){

                            assert(state == "placed", 'should be placed');
                            return cb();

                        });

                    });

            }

        ], function (err) {

            if (err) {

                assert.isNull(err, 'No errors');
            }

            done();


        });

    });


    it('delete order should throw', function (done) {

        async.series([

            function (cb) {

                db.order.model.findOne()
                    .populate('orderDetails')
                    .exec(function (err, order) {

                            order.remove(function (err) {
                                return cb(err);
                            });

                    });

            }

        ], function (err) {


            assert.isNotNull(err, 'thrown when trying to hard remove order');
            done();


        });

    });


    it('cancel pending order', function (done) {

        async.series([

            function (cb) {

                db.order.model.findOne()
                    .exec(function (err, order) {

                        order.cancelled = true;
                        order.save(function (err, saved) {
                            return cb(err, saved);
                        });

                    });

            },
            
            function(cb){
                
                setTimeout(function(){
                    db.order.model.count()
                        .where('notifiedCancel', true)
                        .exec(function(err, count){
                           if(err || count === 1){
                               return cb(err);
                           } 
                            assert.isNotNull('','Driver notification not sent');
                            return cb();
                            
                        });
                }, 3000);
            }

        ], function (err,result) {

            var order = result[0];

            assert.isNull(err, 'error is null');
            assert.isNotNull(order, 'order exists');


            done();


        });

    });


    it('try to cancel non-pending order', function (done) {

        async.series([

            function (cb) {

                db.order.model.findOne()
                    .exec(function (err, order) {

                        order.cancelled = true;
                        order.save(function (err, saved) {
                            return cb(err, saved);
                        });

                    });

            },

            function(cb){
                
                db.order.model.findOne()
                    .exec(function (err, order) {

                        order.cancelled = true;
                        order.save(function (err, saved) {
                            return cb(err, saved);
                        });

                    });
            }

        ], function (err,result) {

            var order = result[1];

            assert.isNotNull(err, 'Can not cancel an order that is in progress');
            assert.isNotObject(order, 'saved order should not exist');
            
            done();

        });

    });

    it('create platform', function (done) {
        
        var platform = new db.platform.model();
        platform.title = "title";
        platform.save(function (err, saved) {

            db.platform.model.findOne(
                {id: saved._id}, function (err) {
                    done();
                    console.log(err);
                }
            )
        });
    });

    it('schema from scratch', function (done) {

        var m = require('mongoose');
        var c = m.createConnection('mongodb://localhost/test');

        c.once('open', function callback() {
            console.log('open');
        });
        var Cat = c.model('Cat', {name: String});
        var kitty = new Cat({name: 'Zildjian'});
        kitty.save(function (err) {
            done();
            console.log(err);
        });
    });

    it('api req', function (done) {


        var request = require('google-oauth-jwt').requestWithJWT();

        request({
            url: 'https://www.googleapis.com/calendar/v3/users/me/calendarList?showDeleted=true&showHidden=true',
            jwt: {
                // use the email address of the service account, as seen in the API console
                email: '1088612676344-pp51kqgvbg03oiadqmp4au5piif4ianf@developer.gserviceaccount.com',
                // use the PEM file we generated from the downloaded key
                keyFile: __dirname + '/google-key-file.pem',
                // specify the scopes you wish to access - each application has different scopes
                scopes: ['https://www.googleapis.com/auth/calendar']
            }
        }, function (err, res, body) {
            console.log(JSON.parse(body));
            done();
        });

    });

    it('api token', function (done) {

        var auth = require('google-oauth-jwt');

        auth.authenticate({
            email: '1088612676344-pp51kqgvbg03oiadqmp4au5piif4ianf@developer.gserviceaccount.com',
            keyFile: __dirname + '/google-key-file.pem',
            scopes: ['https://www.googleapis.com/auth/calendar']
        }, function (err, token) {
            console.log(token);
            done();
        });

    });

    it('api token cache', function (done) {

        var cache = require('google-oauth-jwt').TokenCache,
            tokens = new cache();

        tokens.get({
            email: '1088612676344-pp51kqgvbg03oiadqmp4au5piif4ianf@developer.gserviceaccount.com',
            keyFile: __dirname + '/google-key-file.pem',
            scopes: ['https://www.googleapis.com/auth/calendar']
        }, function (err, token) {
            console.log(token);
            done();
        });

    });

    it('gcal (add)', function (done) {

        var auth = require('google-oauth-jwt');
        var gcal = require('google-calendar');

        var title = 'Joey Jeremiah';
        var text = 'Work';

        auth.authenticate({
            email: '1088612676344-pp51kqgvbg03oiadqmp4au5piif4ianf@developer.gserviceaccount.com',
            keyFile: __dirname + '/google-key-file.pem',
            scopes: ['https://www.googleapis.com/auth/calendar']
        }, function (err, token) {
            console.log(token);

            var options = {
                "summary": text,
                "selected": true,
                hidden: false
            };
            gcal(token).calendars.insert(options, function(err, result) {

                console.log(JSON.stringify(result));
                //options.id = result.id;
                delete options.summary;
done();
               /* gcal(token).calendarList.insert({id:result.id}, function(err, data) {
                    done();
                });*/
                /*gcal(token).calendars.delete(inserted_calendar_id, function(err, result) {

                    console.log(JSON.stringify(result));
                    done()
                })*/
            })
        });

    });

    it('gcal (list)', function (done) {

        var auth = require('google-oauth-jwt');
        var gcal = require('google-calendar');

        auth.authenticate({
            email: '1088612676344-pp51kqgvbg03oiadqmp4au5piif4ianf@developer.gserviceaccount.com',
            keyFile: __dirname + '/google-key-file.pem',
            scopes: ['https://www.googleapis.com/auth/calendar']
        }, function (err, token) {
            console.log(token);
            //gcal(token).calendarList.list(function(err, data) {
               // console.log(data);
                done();
            //});
        });

    });

});
