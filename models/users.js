var keystone = require('keystone'),
    mongoose = require('mongoose'),
    Types = keystone.Field.Types,
    _ = require('underscore'),
    speakeasy = require('speakeasy'),
    client = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH_TOKEN);

var lengthValidator = function (val) {
    if (val && val.length >= 8) {
        return true;
    }
    return false;
};

/**
 * Users Model
 * ===========
 */

var User = new keystone.List('User', {
    autokey: {
        path: 'key',
        from: 'email',
        unique: true
    },
    track: true
});

var deps = {
    github: {'services.github.isConfigured': true},
    facebook: {'services.facebook.isConfigured': true},
    twitter: {'services.twitter.isConfigured': true}
};

User.add({
    name: {
        type: Types.Name,
        required: true,
        index: true,
        unique: false
    },
    email: {
        type: Types.Email,
        initial: true,
        index: true,
        unique: true,
        validate: {
            validator: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i,
            msg: 'Not an email'
        }
    },
    username: {
        type: String,
        initial: true,
        index: true,
        unique: false
    },
    password: {
        type: Types.Password,
        initial: true,
        validate: {
            validator: lengthValidator,
            msg: 'Password too short'
        }
    },
    phone: {
        type: String,
        initial: true
    },
    resetPasswordKey: {
        type: String,
        hidden: true
    },
    state: {
        type: Types.Select,
        options: 'disabled, valid, archived',
        default: 'valid',
        index: true
    }
}, 'Verifications', {
    verifications: {
        email: {
            type: Boolean,
            hidden: true,
            default: false
        },
        verifyEmailKey: {
            type: String,
            hidden: true
        },
        phone: {
            type: Boolean,
            hidden: true,
            default: false
        },
        verifyPhoneKey: {
            type: String,
            hidden: true
        }
    }
}, 'Profile', {
    photo: {type: Types.CloudinaryImage},
    twitter: {
        type: String,
        width: 'short'
    }
}, 'Notifications', {
    notifications: {
        native: {
            type: Boolean,
            default: false
        },
        inApp: {
            type: Boolean,
            default: false
        },
        email: {
            type: Boolean,
            default: false
        }
    }
}, 'Permissions', {
    isAdmin: {
        type: Boolean,
        label: 'Can Admin HomeMade'
    }
}, 'orderDefaults', {
    defaults: {
        washFold: {
            useFabricSoftener: {
                type: Boolean,
                default: false,
                label: 'Use fabric softener'
            },
            useGreenDetergent: {
                type: Boolean,
                default: false,
                label: '+10% to order, we use unscented detergent'
            },
            doSeperateWhites: {
                type: Boolean,
                default: false,
                label: '+$1.99 per load'
            },
            doBleachWhites: {
                type: Boolean,
                default: false,
                label: '+$1.99 per load, requires whites are washed separately'
            },
            specialInstructions: {
                type: String,
                label: 'Provide us special instructions for your wash and fold laundry'
            }
        },
        washPress: {
            useGreenDetergent: {
                type: Boolean,
                default: false,
                label: '+10% per item, we use unscented detergent.'
            },
            starchLevel: {
                type: Types.Select,
                options: 'none, light, medium, heavy',
                default: 'none',
                label: 'How much starch do you want applied?'
            },
            shirtPackaging: {
                type: Types.Select,
                options: 'hanger, boxed',
                default: 'hanger',
                label: '+0.50 per boxed shirt'
            },
            specialInstructions: {
                type: String,
                label: 'Provide us special instructions for your pressed laundry.'
            }
        },
        dryClean: {
            useGreenDetergent: {
                type: Boolean,
                default: false,
                label: '+10% per item, we use unscented detergent.'
            },
            starchLevel: {
                type: Types.Select,
                options: 'none, light, medium, heavy',
                default: 'none',
                label: 'How much starch do you want applied?'
            },
            shirtPackaging: {
                type: Types.Select,
                options: 'hanger, boxed',
                default: 'hanger',
                label: '+0.50 per boxed shirt'
            },
            specialInstructions: {
                type: String,
                label: 'Provide us special instructions for your dry cleaned laundry.'
            }
        }
    }
}, 'Services', {
    services: {
        facebook: {
            isConfigured: {
                type: Boolean,
                label: 'Facebook has been authenticated'
            },

            profileId: {
                type: String,
                label: 'Profile ID',
                dependsOn: deps.facebook
            },
            profileUrl: {
                type: String,
                label: 'Profile URL',
                dependsOn: deps.facebook
            },

            username: {
                type: String,
                label: 'Username',
                dependsOn: deps.facebook
            },
            accessToken: {
                type: String,
                label: 'Access Token',
                dependsOn: deps.facebook
            }
        },
        twitter: {
            isConfigured: {
                type: Boolean,
                label: 'Twitter has been authenticated'
            },

            profileId: {
                type: String,
                label: 'Profile ID',
                dependsOn: deps.twitter
            },

            username: {
                type: String,
                label: 'Username',
                dependsOn: deps.twitter
            },
            accessToken: {
                type: String,
                label: 'Access Token',
                dependsOn: deps.twitter
            }
        }
    },
    locations: {type: Types.Relationship, ref: 'Location', many: true}
});


/**
 Relationships
 =============
 */
    //User.relationship({ref    : 'Location', refPath: 'User', path   : 'locations'});

User.relationship({ref: 'Location', refPath: 'user', path: 'locations'});

/**
 * Hooks
 * =====
 */

var _original;

User.schema.pre('save', function (next) {
    var self = this;

    if (self.primary) {
        keystone.list('Location').model.find()
            .where('user', self.user._id)
            .where('status').in(['initial', 'unverified', 'verified'])
            .where('primary', true)
            .exec(function (err, loc) {

                if (err) return next(err);

                if (loc.length > 1) return next(new Error('User has more than one primary location'));

                if (loc.length === 0) return next();

                loc[0].primary = false;
                loc[0].save(function (err) {

                    if (err) return next(err);

                    return next();

                });

            });
    }
    next();

});

/*User.schema.post('init', function () {
 this._original = this.toObject();
 });*/

User.schema.pre('remove', function (next) {
    return next(new Error("You can't delete a user."));
});

/*User.schema.pre('validate', function (next) {

 if(!this._original) return next();

 if(_original.phone !== this.phone) {
 this.verifications.phone = false;
 this.verifications.verifyPhoneKey = "";
 }

 if(_original.email !== this.email) {
 this.verifications.email = false;
 this.verifications.verifyEmailKey = "";
 }

 next();
 });*/


/**
 * Virtuals
 * ========
 */

    // Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
    return this.isAdmin;
});

User.schema.virtual('isVerified').get(function () {
    return this.verifications.email &&
        this.verifications.phone &&
        this.state === 'valid';
});

User.schema.virtual('isGettingNotifications').get(function () {
    return this.notifications.native ||
        this.notifications.inApp ||
        this.notifications.email;
});

User.schema.virtual('sanitized').get(function () {
    // make sure all the relationships are loaded
    var self = this;

    var user = {
        email: this.email,
        username: this.username,
        name: this.name,
        phone: this.phone,
        isValid: this.state === 'valid',
        isVerified: this.isVerified,
        verifications: {
            phone: this.verifications.phone,
            email: this.verifications.email
        },
        isGettingNotifications: this.isGettingNotifications,
        notifications: {
            native: this.notifications.native,
            inApp: this.notifications.inApp,
            email: this.notifications.email
        },
        orderDefaults: {
            washFold: {
                useFabricSoftener: this.defaults.washFold.useFabricSoftener,
                useGreenDetergent: this.defaults.washFold.useGreenDetergent,
                doSeperateWhites: this.defaults.washFold.doSeperateWhites,
                doBleachWhites: this.defaults.washFold.doBleachWhites,
                specialInstructions: this.defaults.washFold.specialInstructions
            },
            washPress: {
                useGreenDetergent: this.defaults.washPress.useGreenDetergent,
                starchLevel: this.defaults.washPress.starchLevel,
                shirtPackaging: this.defaults.washPress.shirtPackaging,
                specialInstructions: this.defaults.washPress.specialInstructions
            },
            dryClean: {
                useGreenDetergent: this.defaults.dryClean.useGreenDetergent,
                starchLevel: this.defaults.dryClean.starchLevel,
                shirtPackaging: this.defaults.dryClean.shirtPackaging,
                specialInstructions: this.defaults.dryClean.specialInstructions
            }
        },
        isAdmin: this.isAdmin,
        locations: []
    };

    _.each(this.locations, function (loc) {
        user.locations.push(loc.sanitized);
    });

    return user;
});


/**
 * Methods
 * =======
 */

User.schema.methods.resetEmailVerification = function (callback) {
    var user = this;
    user.verifications.email = false;
    user.verifications.verifyEmailKey = speakeasy.hotp({
        key: '53cR3tz',
        length: 4,
        counter: Math.floor(Math.random() * new Date().getTime())
    });

    new keystone.Email('user-email-verification').send({
        name: user.name.first || user.name.full,
        code: user.verifications.verifyEmailKey,
        brand: keystone.get('brand'),
        link: keystone.get('open app ios'),
        subject: 'Verify your email',
        to: user,
        from: {
            name: 'Support',
            email: 'contact@homemadehub.com'
        }
    }, callback);
}


User.schema.methods.resetPhoneVerification = function (callback) {
    var user = this;
    user.verifications.phone = false;
    user.verifications.verifyPhoneKey = speakeasy.hotp({
        key: '53cR3tz',
        length: 4,
        counter: Math.floor(Math.random() * new Date().getTime())
    });

    client.messages.create({
        to: user.phone,
        from: process.env.TWILIO_NUMBER,
        body: 'To use ' + keystone.get('brand') +
        ' services enter this verification code into the app. ' +
        user.verifications.verifyPhoneKey
    }, function (twilioerr, responseData) {
        if (twilioerr) {
            callback(twilioerr);
        }
        else {

            callback(null);
        }
    });
}

User.schema.methods.resetPassword = function (callback) {
    var user = this;

    user.resetPasswordKey = speakeasy.hotp({
        key: '53cR3tz',
        length: 24,
        counter: Math.floor(Math.random() * new Date().getTime())
    });

    user.save(function (err) {

        if (err) {
            return callback(err);
        }

        new keystone.Email('forgotten-password').send({
            name: user.name.first || user.name.full,
            link: 'https://homemadehub.com/reset-password/' +
            user.resetPasswordKey,
            subject: 'Reset your Password'
        }, {
            to: user,
            from: {
                name: 'Support',
                email: 'contact@homemadehub.com'
            }
        }, callback);

    });

}

/**
 * Registration
 * ============
 */

User.defaultColumns = 'name, email, twitter, isAdmin';
User.register();
