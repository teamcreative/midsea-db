var keystone = require('keystone'),
    _ = require('underscore'),
    Types = keystone.Field.Types;

/**
 * Location Model
 * =============
 */

var Location = new keystone.List('Location', {
    autokey: { path: 'slug', from: 'name user', unique: true },
    track: true
});

Location.add({
    name: {type: String, required: true, initial: true},
    status: {type: Types.Select, options: 'initial, unverified, verified, bogus, removed', default: 'initial'},
    place: {type: Types.Location, required: true, initial: true},
    instructions: {type: Types.Html, wysiwyg: true},
    user: {type: Types.Relationship, ref: 'User'},
    primary: {type: Boolean, intial: true, default: false}
});


/**
 * Relationships
 * =============
 */


/**
 * Hooks
 * =====
 */

Location.schema.pre('save', function (next) {
    var self = this;

    keystone.list('User').model.findOne({'_id': self.user}, function (err, user) {
        if (err) {

            return next(err);

        }

        if (!user) {

            // LOG - should always have a user
            return next("Location couldn't lookup the user");

        }

        user.locations.push(self);
        user.save(function (err) {
            if (err) {

                // LOG
                return next(err);

            }

            if (self.primary) {
                keystone.list('Location').model.find()
                    .where('user', self.user._id)
                    .where('status').in(['initial', 'unverified', 'verified'])
                    .where('primary', true)
                    .exec(function (err, loc) {

                        if (err) return next(err);

                        if (loc.length > 1) return next(new Error('User has more than one primary location'));

                        if (loc.length === 0) return next();

                        loc[0].primary = false;
                        loc[0].save(function (err) {

                            if (err) return next(err);

                            return next();

                        });

                    });
            } else {
                return next();
            }

        });
    });

});

Location.schema.post('save', function () {

});

Location.schema.pre('remove', function (next) {

    return next(new Error("Can't delete a location, set status to 'removed' instead"));
});

Location.schema.pre('validate', function (next) {

    next();
});


/**
 * Virtuals
 * ========
 */

Location.schema.virtual('sanitized').get(function () {
    return {
        'name': this.name,
        'status': this.status,
        'instructions': this.instructions,
        'isPrimary': this.primary,
        'address': {
            'unit': this.place.street2,
            'address': this.place.number + ' ' + this.place.street1,
            'postal': this.place.postcode,
            'city': this.place.suburb,
            'province': this.place.state,
            'geo': this.place.geo
        }
    }
});

Location.schema.virtual('getUserLocations').get(function () {
    if (!this.user) return null;

    var results = [];

    keystone.list('Location').model.find()
        .where('user', this.user)
        .where('status').nin('removed')
        .sort('updatedAt')
        .exec(function (err, locs) {

            if (err) return null;

            _.forEach(locs, function (loc) {
                results.push(loc.sanitized);
            });

            return results;
        });
});


/**
 * Methods
 * =======
 */


/**
 * Registration
 * ============
 */

Location.defaultColumns = 'name, state|20%, date|20%';
Location.register();
