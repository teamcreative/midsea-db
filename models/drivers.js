var keystone = require('keystone'),
    Types = keystone.Field.Types,
    _ = require('underscore'),
    speakeasy = require('speakeasy'),
    utils = require('../modules/utils.js'),
    client = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_AUTH_TOKEN);

var lengthValidator = function (val) {
    if (val && val.length >= 8) {
        return true;
    }
    return false;
};

/**
 * Driver Model
 * ===========
 */

var Driver = new keystone.List('Driver', {
    autokey: {path: 'key', from: 'name', unique: true},
    track: true
});

Driver.add({
    name: {type: Types.Name, required: true, index: true},
    email: {
        type: Types.Email, initial: true, index: true, unique: false,
        validate: {
            validator: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i,
            msg: 'Not an email'
        }
    },
    phone: {type: String, initial: true},
    state: {type: Types.Select, options: 'disabled, valid, archived', default: 'valid', index: true},
    calendarHandle: {type: String, initial: true},
    details: {type: Types.Html, wysiwyg: true},
    location: Types.Location
}, 'Profile', {
    photo: {type: Types.CloudinaryImage},
    startDate: {type: Date},
    finishDate: {type: Date}
});


/**
 Relationships
 =============
 */


Driver.relationship({ref: 'Order', refPath: 'pickupDriver', path: 'pickups'});
Driver.relationship({ref: 'Order', refPath: 'dropoffDriver', path: 'dropoffs'});
Driver.relationship({ref: 'Order', refPath: 'schduledPickupDriver', path: 'scheduledPickups'});
Driver.relationship({ref: 'Order', refPath: 'scheduledDropoffDriver', path: 'scheduledDropoffs'});

/**
 * Hooks
 * =====
 */

Driver.schema.pre('save', function (next) {
    next();

});

Driver.schema.pre('remove', function (next) {
    return next(new Error("You can't delete a driver."));
});

Driver.schema.pre('validate', function (next) {
    next();
});


/**
 * Virtuals
 * ========
 */


Driver.schema.virtual('isVerified').get(function () {
    return null;
});


/**
 * Methods
 * =======
 */

Driver.schema.methods.refreshPickups = function (callback) {


}

// TODO - change to scheduled process
Driver.schema.methods.notifyPickupCancelled = function (callback) {

    var driver = this;

    keystone.list('Order').model.find()
        .where('cancelled', true)
        .where('notifiedCancel', false)
        .where('scheduledPickupDriver', driver)
        .populate('location')
        .exec(function (err, orders) {

            if (err) return callback(err);

            //var order = null;

            while ((order = orders.pop()) ) {
                
                var self = order;

                if (err) return callback(err);
                

                // notify driver
                client.messages.create({
                    to: driver.phone,
                    from: process.env.TWILIO_NUMBER,
                    body: 'Cancelled ' + utils.formatLocation(self.location.place)
                }, function (err, resp) {
                    if (err) {
                        // TODO - LOG & EMAIL
                        callback(err);
                    }
                    else {
                        self.notifiedCancel = true;
                        self.save(function (err) {
                            if (err) {
                                // TODO - LOG & EMAIL
                            }

                            if (orders.length === 0) {
                                return callback();
                            }

                        });
                    }
                });
            }

        });
}

/**
 * Registration
 * ============
 */

Driver.defaultColumns = 'name, email';
Driver.register();
