var keystone = require('keystone'),
    async = require('async'),
    hash = require('mongoose-hash'),
    Types = keystone.Field.Types;

/**
 * Posts Model
 * ===========
 */

var Platform = new keystone.List('Platform', {
  autokey: { from: 'publicKey', path: 'key', unique: true },
  track: true
});

Platform.add({
  /* defaulted */
  version: {type: Number, default: 1},
  publicKey: {type: String, required: true, unique: true, initial: false},
  privateKey: {type: String, required: true, unique: true, initial: false},
  state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
  author: { type: Types.Relationship, ref: 'User', index: true },
  publishedDate: { type: Types.Date, index: true },

  /* editable */
  title: { type: String, required: true, initial: true },
  image: { type: Types.CloudinaryImage },
  isPublic: { type: Boolean, default: true},
  content: {
    brief: { type: Types.Markdown, height: 150 },
    extended: { type: Types.Html, wysiwyg: true, height: 400 }
  }}, 'Categories', {
  categories: {
    platformType: { type: Types.Relationship, ref: 'PlatformType', many: false }
  }
}, 'Features', {
  features: {
    customer: {type: Boolean, default: false},
    cleaner: {type: Boolean, default: false},
    driver: {type: Boolean, default: false},
    admin: {type: Boolean, default: false}
  }
});

/**
 * Virtuals
 * ========
 */

Platform.schema.virtual('content.full').get(function ()
{
  return this.content.extended || this.content.brief;
});

Platform.schema.plugin(hash, {
  field: 'publicKey',
  size: 16
});

Platform.schema.plugin(hash, {
  field: 'privateKey',
  size: 24
});

Platform.schema.virtual('sanitized').get(function ()
{
  var platform = {};

  switch (this.version)
  {
    case 1:
      platform = {
        "version": this.version,
        "enabled": this.state == 'published',
        "customer": this.features.customer,
        "cleaner": this.features.cleaner,
        "driver": this.features.driver,
        "admin": this.features.admin,
        "privateKey": this.privateKey,
        "publicKey": this.publicKey
      };
      break;
  }

  return platform;

});

/**
 * Hooks
 * =====
 */

/*Platform.schema.pre('save', function(next) {
 this.publickey = Date.now();
 this.privatekey =
 next();
 });*/

Platform.schema.post('init', function () {
    this._original = this.toObject();
});

Platform.schema.pre('remove', function (next) {
    if (this._original.state === 'published') {
        return next(new Error("You can't delete a 'Published' platform."));
    }
    next();
});

Platform.schema.pre('validate', function (next) {
    var self = this;

    /*if(this._original && this._original.state &&
        this._original.state === 'published'){
        return next(new Error("You can't modify a 'Published' platform in any way."));
    }*/

    if (self.state === 'published') {
        if(this._original && this._original.state === 'draft'){
            // admin must be approving so send user an email
            this.notifyApproval();
        }
    }

    next();
});




/**
 * Relationships
 * =============
 */


/**
 * Notifications
 * =============
 */

Platform.schema.methods.notifyAdmins = function (callback)
{

  var platform = this;

  // Method to send the notification email after data has been loaded
  var sendEmail = function (err, results)
  {

    if (err) return callback(err);

    async.each(results.admins, function (admin, done)
    {

      new keystone.Email('admin-notification-new-platform').send({
        admin: admin.name.first || admin.name.full,
        author: results.author ? results.author.name.full : 'Somebody',
        title: platform.title,
        keystoneURL: 'https://homemadehub.com/keystone/platforms/' + platform.id,
        subject: 'New Platform'
      }, {
        to: admin,
        from: {
          name: 'Support',
          email: 'contact@homemadehub.com'
        }
      }, done);

    }, callback);

  }

  // Query data in parallel
  async.parallel({
    author: function (next)
    {
      if (!platform.author) return next();
      keystone.list('User').model.findById(platform.author).exec(next);
    },
    admins: function (next)
    {
      keystone.list('User').model.find().where('isAdmin', true).exec(next)
    }
  }, sendEmail);

};


Platform.schema.methods.notifyApproval = function (callback) {
    var platform = this;

    keystone.list('User').model.findById(platform.author).exec()
        .then(function (user) {
            platform.author = user;
        }, function (err) {
            throw err;
        }).then(function () {
            new keystone.Email('user-notification-approved-platform').send({
                name: platform.author.name.first || platform.author.name.full,
                link: 'https://homemadehub.com/platforms/' + platform.id,
                title: platform.title,
                subject: platform.title + ' has been approved'
            }, {
                to: user,
                from: {
                    name: 'Support',
                    email: 'contact@homemadehub.com'
                }
            }, callback);
        }, function (err) {
            throw err;
        });
};
/**
 * Registration
 * ============
 */

Platform.defaultSort = '-publishedDate';
Platform.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
Platform.register();
