var keystone = require('keystone'),
    async = require('async'),
    _ = require('underscore'),
    Types = keystone.Field.Types;

/**
 * Model
 * ===========
 */

var Order = new keystone.List('Order', {
    track: true
});

Order.add({
    pickupDriver: {type: Types.Relationship, ref: 'Driver'},
    scheduledPickupDriver: {type: Types.Relationship, ref: 'Driver'},
    dropoffDriver: {type: Types.Relationship, ref: 'Driver'},
    scheduledDropoffDriver: {type: Types.Relationship, ref: 'Driver'},
    location: {type: Types.Relationship, ref: 'Location'},
    who: {type: Types.Relationship, ref: 'User', required: true, initial: true, index: true},
    cancelled: {type: Types.Boolean, default: false},
    notifiedCancel: {type: Types.Boolean, default: false},
    instructions: {type: String},
    orderDetails: {type: Types.Relationship, ref: 'OrderDetail', many: true},
    promoCode: {type: String}/*,
     createdAt: {type: Date, noedit: true, collapse: true, default: Date.now},
     changedAt: {type: Date, noedit: true, collapse: true}*/
});

/**
 Relationships
 =============
 */

Order.relationship({ref: 'OrderDetail', refPath: 'order', path: 'orderDetails'});


/**
 * Hooks
 * =====
 */

Order.schema.pre('save', function (next) {

    var self = this;

    if (self.orderDetails.length === 0) {

        /* add initial details */
        var detail = new (keystone.list('OrderDetail').model)();
        detail.order = self;
        detail.save(function (err, saved) {

            if (err) {
                // LOG
                return next(err);
            }

            self.orderDetails.push(saved);

            return next();
        });

    } else {

        self.getCurrentState(function (state) {

            if (state !== 'placed' && self.cancelled) {
                return next(new Error("You can't cancel an order that is in progress."));
            }

            keystone.list('OrderDetail').model.find()
                .where('order', self)
                .exec(function (err, details) {

                    if (err) {

                        return next(err);

                    }

                    if (!details || details.length === 0) {

                        // LOG - should always have a detail
                        return next(new Error('This order must have order details'));

                    }

                    self.orderDetails = [];
                    _.each(details, function (detail) {
                        self.orderDetails.push(detail);
                    });
                    
                    if(state == 'placed' /*&& self.notifiedCancel*/ && self.cancelled) {
                        
                        var detail = new (keystone.list('OrderDetail').model)();
                        detail.order = self;
                        detail.state = 'cancelled';
                        detail.save(function (err, saved) {

                            if (err) {
                                // LOG
                                return next(err);
                            }

                            self.orderDetails.push(saved);

                            return next();
                        });
                        
                    } else {

                        return next();
                    }

                });
        });
    }
});


Order.schema.post('save', function () {

    var self = this;

    this.getCurrentState(function (state) {

        if (state === 'placed' && self.cancelled) {

            keystone.list('Driver').model.findById(self.scheduledPickupDriver, function (err, driver) {

                if (driver) {
                    driver.notifyPickupCancelled(function (err) {
                        if (err) {
                            // should already be handled
                        }
                    });
                }
            });
        }
    });
});

Order.schema.pre('remove', function (next) {
    return next(new Error("You can't delete an order."));
});


/**
 * Virtuals
 * ========
 */


Order.schema.methods.getCurrentState = function (cb) {
    var self = this;

    keystone.list('OrderDetail').model.find()
        .where('order', self)
        .exec(function (err, details) {

            if (err) {

                throw err;

            }

            if (!details || details.length === 0) {

                // LOG - should always have a detail
                return cb(null);

            }

            var latest = _.max(details,
                function (detail) {
                    return detail.createdAt;
                });

            return cb(latest.state);
        });
};


/**
 * Registration
 * ============
 */

Order.defaultColumns = 'who, createdAt';
Order.defaultSort = '-createdAt';
Order.register();
