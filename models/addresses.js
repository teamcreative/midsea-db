var keystone = require('keystone'),
    async = require('async'),
    Types = keystone.Field.Types;

/**
 * Posts Model
 * ===========
 */

var Address = new keystone.List('Address', {
  autokey: { from: 'publicKey', path: 'key', unique: true },
  track: true
});

Address.add({
  /* defaulted */
    suiteName: { type: String, required: false, unique: false, initial: false },
    unit: { type: String, required: false, unique: false, initial: false },
    building: { type: String, required: false, unique: false, initial: false },
    direction: { type: String, required: false, unique: false, initial: false },
    street: { type: String, required: false, unique: false, initial: false },
    fullAddress: { type: String, required: false, unique: false, initial: false },
    vacant: { type: Boolean, required: false, unique: false, initial: false },
    city: { type: String, required: false, unique: false, initial: false },
    province: { type: String, required: false, unique: false, initial: false },
    postalCode: { type: String, required: false, unique: false, initial: false },
    country: { type: String, required: false, unique: false, initial: false },
    latitude: {type: Number, required: false, unique: true, initial: false},
    longitude: {type: Number, required: false, unique: true, initial: false}
});

/**
 * Virtuals
 * ========
 */


/**
 * Hooks
 * =====
 */


/**
 * Relationships
 * =============
 */


/**
 * Notifications
 * =============
 */

/**
 * Registration
 * ============
 */

Address.defaultSort = '-city';
Address.defaultColumns = 'fullAddress';
Address.register();
