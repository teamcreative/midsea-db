var keystone = require('keystone'),
    Types = keystone.Field.Types;

/**
 * Post Categories Model
 * =====================
 */

var PlatformType = new keystone.List('PlatformType', {
  autokey: { from: 'name', path: 'key', unique: true },
  client: { noedit: true, nocreate: true, nodelete: true, noview: false },
  track: true
});

PlatformType.add({
  name: { type: String, required: true },
  platform: { type: Types.Relationship, ref: 'Platform', many: true }
});


/**
 * Relationships
 * =============
 */

PlatformType.relationship({ ref: 'Platform', refPath: 'platformtype', path: 'platforms' });


/**
 * Registration
 * ============
 */

//PlatformType.addPattern('standard meta');
PlatformType.register();
