var keystone = require('keystone'),
    async = require('async'),
    _ = require('underscore'),
    Types = keystone.Field.Types;

/**
 * Model
 * ===========
 */

var OrderDetail = new keystone.List('OrderDetail', {
    track: true
});

OrderDetail.add({
    order: {type: Types.Relationship, ref: 'Order', required: true, initial: true},
    state: {
        type: Types.Select,
        options: 'placed, pickedup, atcleaners, enroute, delivered-Person, delivered-Drop, delivered-Missed, complete, archived, cancelled',
        default: 'placed',
        index: true,
        initial: true,
        required: true
    },
    notes: {type: Types.Html, wysiwyg: true}
});


/**
 Relationships
 =============
 */


/**
 * Hooks
 * =====
 */

OrderDetail.schema.pre('save', function (next) {

    next();

});

OrderDetail.schema.post('save', function () {
    /*keystone.list('Order').model.findOne()
        .where('order', this.order.id)
        .exec(function (err, order) {
            if (err) throw err;

            if (!order) throw "The Order doesn't exist";
        });*/
});

OrderDetail.schema.pre('remove', function (next) {

    return next(new Error("You can't delete an order."));

});


/**
 * Registration
 * ============
 */

OrderDetail.defaultColumns = 'order, createdAt';
OrderDetail.defaultSort = '-createdAt';
OrderDetail.register();
